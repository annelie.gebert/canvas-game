import {Bubble} from "./bubble.js";
import {Flower} from "./flower.js";

const canvas = document.getElementById("myCanvas"); // grab the canvas
const ctx = canvas.getContext("2d"); // get the context 2d

const bubbleList = [];
const flowerList = [];
const flowerareaList = [];

const flowerNumber = 5;
const flowerareaHeight = 10;
const flowerareaWidth = 40;
const areaDistance = 100;

const flowerHeight = 70;
const flowerWidth = 70;
const flowerDistance = 85;
const grownFlowerHeight = flowerHeight + 5*20;   //

const bugProbability = 0.2; // probability for a bubble containing a bug
const bugAudio = new Audio('/audio/bug.wav')
const dropv = 3;

let counter = 0;            // counts the full-grown flowers
let gameOver = false;
let drawCounter = 0;

// ------------------------------------------------------------------------------------------------
// EventListener: when you klick into a bubble, it turns into a drop or the bug is set free
// ------------------------------------------------------------------------------------------------

canvas.addEventListener("click",clickHandler);

function clickHandler(e) {
    let xclick = e.clientX - canvas.offsetLeft;
    let yclick = e.clientY - canvas.offsetTop;

    for (const bubble of bubbleList) {
        if (!gameOver &&
            Math.pow(bubble.x - xclick, 2) + Math.pow(bubble.y - yclick, 2)
            <= Math.pow(bubble.radius, 2)
        ) {
            if (bubble.bug) {
                bugAudio.play().then();
                bubble.status = 2; // set the bug free
            } else {
                bubble.status = 0; // turn into a drop
            }
        }
    }
}

// ------------------------------------------------------------------------------------------------
// Object Construction Functions
// ------------------------------------------------------------------------------------------------

function Flowerarea(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

// ------------------------------------------------------------------------------------------------
// Initialising Lists
// ------------------------------------------------------------------------------------------------

function initFlowerList(flowerHeight, flowerWidth, distance, flowerNumber) {

    // calculate the space between the flowers
    let spaceBetween = (canvas.width - 2*distance - flowerNumber*flowerWidth) / (flowerNumber - 1)

    // fill the flowerList with flowers
    for (let i = 0; i < flowerNumber; i++) {
        flowerList.push(
            new Flower(
                distance + i*(flowerWidth + spaceBetween),
                canvas.height - flowerHeight - flowerareaHeight,
                flowerWidth,
                flowerHeight,
                'stay'
            )
        )
    }
}

function initFlowerAreaList(flowerNumber, flowerareaHeight, flowerareaWidth, distance) {

    // calculate the space between the flowerareas
    let spaceBetween = (canvas.width - 2*distance - flowerNumber*flowerareaWidth) / (flowerNumber - 1)

    // fill the flowerarealist
    for (let i = 0; i < flowerNumber; i++) {
        flowerareaList.push(
            new Flowerarea(
                distance + i*(flowerareaWidth + spaceBetween),
                canvas.height - flowerareaHeight,
                flowerareaWidth,
                flowerareaHeight
            )
        )
    }
}

function addBubble() {
    const radius = 20 + Math.random()*20;
    const bubble = new Bubble(
        600 + Math.random()*100,
        150 - Math.random()*100,
        radius,
        hasBug()
    );

    bubble.vx = 1/radius * 40;          // the smaller the bubble, the faster it moves
    bubble.vy = getRandomYv(bubble);
    bubbleList.push(bubble);
}

// ------------------------------------------------------------------------------------------------
// Helper Functions
// ------------------------------------------------------------------------------------------------

function hasBug() {
    return Math.random() < bugProbability;
}

function getRandomYv(bubble) {
    return (-0.5 + Math.random()) / bubble.radius * 40;
}

function tidyUpBubbleList() {
    for (let i=0; i < bubbleList.length; i++) {
        let bubble = bubbleList[i];
        if (bubble.x < -bubble.radius ||
            bubble.y > canvas.height + bubble.radius) {
            bubbleList.splice(i, 1);
        }
    }
}

function generateBubblesVy() {
    for (const bubble of bubbleList) {
        if (bubble.status === 1) {
            setTimeout(getRandomYv, Math.random()*500);
        }
    }
}

function turnAlltoDrops() {
    for (const bubble of bubbleList) {
        if (bubble.bug) {
            bubble.status = 2;
        }
        else {
            bubble.status = 0;
        }
    }
}

// ------------------------------------------------------------------------------------------------
// Collision of the bugs and drops with the flowers
// ------------------------------------------------------------------------------------------------

function collisionDetection() {
    let b = -1;
    for (const bubble of bubbleList){
        b++;
        let i = -1;
        for (const area of flowerareaList) { // check for every bubble, if it hits a flower
            i++;

            if (bubble.x >= area.x &&                       // if a bug or a drop hits the flowerarea
                bubble.x <= area.x + area.width &&
                flowerList[i].status !== 'grown' &&         // and the flower is not full-grown
                bubble.status !== 3                         // and the bug is hungry and not running
            )
            {
                if (bubble.status === 0 &&                      // in case it is a drop
                    bubble.y >= canvas.height - area.height)    // and it hits the top of the area
                {
                    flowerList[i].status = 'growing';           // the flower starts growing
                    flowerList[i].dropCounter++;
                    bubbleList.splice(b, 1);            // and the drop is deleted
                }
                else if (
                    bubble.status === 2 &&                     // in case it is a bug
                    bubble.y >= canvas.height - flowerList[i].height) // and it hits the top of the flower
                {
                    flowerList[i].status = 'shrinking';         // the flower starts to shrink
                    flowerList[i].dropCounter = 0;
                    if (bubble.y >= canvas.height ){            // and when the bug hits the ground
                        bubbleList.splice(b, 1);      // it is deleted
                    }
                }
            }
        } // end of inner for loop

        if (bubble.bug &&                               // if the bubble contains a bug or turned into one
            bubble.y > canvas.height - bubble.radius)   // and hits the ground
        {
            bubble.status = 3;                          // the bug is not hungry, it will just run
        }
    }
}

// ------------------------------------------------------------------------------------------------
// Draw-Functions to draw the flowers, bubbles, areas and game over
// ------------------------------------------------------------------------------------------------

function drawBackground() {
    let base_image = new Image();
    base_image.src = '/img/background1.jpg';
    ctx.beginPath();
    ctx.drawImage(base_image, 0, 0, 600, 400);
    ctx.closePath();
}

function drawFlowerareas() {
    for (const area of flowerareaList) {
        drawFlowerarea(area.x, area.y, area.width, area.height);
    }
}

function drawFlowerarea(x, y, width, height) {
    ctx.beginPath();
    ctx.rect(x, y, width, height);
    ctx.fillStyle = "#5b370e";
    ctx.fill();
    ctx.closePath();
}

function drawFlower(flower, image) {
    let base_image = new Image();
    base_image.src = '/img/' + image;
    ctx.beginPath();
    ctx.drawImage(base_image, flower.x, flower.y, flower.width, flower.height);
    ctx.closePath();
}

function drawFlowers() {
    let i = -1;
    for (const flower of flowerList) {
        i++;
        if (flower.height < grownFlowerHeight)
        {
            drawFlower(flower, 'tulip.png');
        } else
        {
            drawFlower(flower, 'tulip2.png');
        }
    }
}

function drawGameOver() {
    ctx.font = "16px Arial";
    ctx.fillStyle = "#0095DD";
    ctx.fillText(`Gewonnen`, canvas.width / 2 - 20, 150);
}

// ------------------------------------------------------------------------------------------------
// Draw the canvas again and again ... ( used in setInterval )
// ------------------------------------------------------------------------------------------------

function draw() {
    drawCounter++;
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    tidyUpBubbleList(); // delete all bubbles, that left the canvas

    drawBackground();
    drawFlowerareas();
    drawFlowers();
    collisionDetection();

    // behavior of the bubbles
    for (const bubble of bubbleList) {
        if (bubble.status === 1)            // if bubble is still alive, it moves from right to left
        {
            bubble.draw(ctx, bubble.radius);
            bubble.x -= bubble.vx;
            bubble.y += bubble.vy;
        } else if (bubble.status === 2)     // the bubble set the bug free, which falls down
        {
            bubble.drawBug(ctx, bubble.radius, 0);
            bubble.y += dropv;
        } else if (bubble.status === 0)     // bubble turned into a drop and falls down
        {
            bubble.draw(ctx, bubble.dropradius);
            bubble.y += dropv;
        } else if (bubble.status === 3)    // a bug running on the ground
        {
            bubble.drawBug(ctx, bubble.radius, drawCounter % 10);
            bubble.x++;
        }
    }

    // flowers growing or shrinking
    for (const flower of flowerList) {
        if (flower.status === 'growing')                // when the flower grows
        {
            flower.grow();                                  // let it grow
            if (flower.height === grownFlowerHeight)        // check, if it is grown now
            {
                flower.status = 'grown';                    // if yes, the flower is grown now
                counter++;
            }
            else if (                                       // otherwise check, if it has grown 20px
                flower.height === flowerHeight + 20*flower.dropCounter)
            {
                flower.status = 'stay';                     // if yes, it stops growing
            }
        }
        else if (
            flower.status === 'shrinking' &&            // when the flower shrinks
            flower.height === flowerHeight)             // and reached the origin size
        {
            flower.status = 'stay'                      // it stops shrinking

        } else if (
            flower.status === 'shrinking')              // but otherwise
        {
            flower.shrink();                            // let it shrink
        }
    }

    if (counter === 5) {
        turnAlltoDrops();
        setTimeout(clearInterval, 5000, drawInterval);
        setTimeout(clearInterval, 2000, addBubbleInterval);
        setTimeout(drawGameOver, 3000);
    }
}

// ------------------------------------------------------------------------------------------------
// Starting the programm
// ------------------------------------------------------------------------------------------------

initFlowerAreaList(flowerNumber, flowerareaHeight, flowerareaWidth, areaDistance);
initFlowerList(flowerHeight, flowerWidth, flowerDistance, flowerNumber);

let drawInterval = setInterval(draw, 20);
let addBubbleInterval = setInterval(addBubble, 1000);

setInterval(generateBubblesVy, 1000);       // for the random change of the y-direction
