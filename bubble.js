
export class Bubble {
    constructor(x, y, radius, bug) {
        this.x = x;
        this.y = y;
        this.vx = 1;        // velocity in x-direction of the bubble
        this.vy = 2;        // velocity in y-direction of the bubble
        this.radius = radius;
        this.dropradius = Math.round(this.radius * 0.2);
        this.color = "rgba(155,155,227,0.5)";
        this.status = 1;    // 3: the bug is not hungry and just crawling around
                            // 2: hungry bug is free,
                            // 1: bubble is flying,
                            // 0: bubble turned into a drop

        this.bug = bug;     // boolean: true, when the bubble contains a bug
    }

    draw(ctx, radius) {
        ctx.beginPath();
        ctx.arc(this.x, this.y, radius, 0, Math.PI * 2, true);
        ctx.closePath();
        ctx.fillStyle = this.color;
        ctx.fill();

        if (this.bug) {
            this.drawBug(ctx, radius);
        }
    }

    drawBug(ctx, radius, picNumber){
        let bug_image = new Image();
        if (this.status === 3) {
            if (picNumber > 5) {
                bug_image.src = '/img/' + 'runningBug' + 0 + '.png';
            } else {
                bug_image.src = '/img/' + 'runningBug' + 1 + '.png';
            }
        } else {
            bug_image.src = '/img/' + 'bug.png';
        }

        ctx.beginPath();
        ctx.drawImage(bug_image, this.x-radius, this.y-radius, radius*2, radius*2);
        ctx.closePath();
    }
}
