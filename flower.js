export class Flower{
    constructor(x, y, width, height, status) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.status = status;   // 'stay' 'growing' 'shrinking' 'full-grown'
        this.dropCounter = 0;   // how many drops have fallen on the flower
    }

    grow() {
        this.height += 1;
        this.width += 1;
        this.y -= 1;
        this.x -= 0.5;
    }

    shrink() {
        this.height -= 1;
        this.width -= 1;
        this.y += 1;
        this.x += 0.5;
    }
}